image: "registry.gitlab.com/cipres/ipfs-void-storm:latest"

variables:
  VENV: $CI_PROJECT_DIR/venv

  ARTIFACT_ENV: "icapsule-env.sh"
  ARTIFACT_DIST: "icapsule-dist.sh"
  ARTIFACT_DEPLOYMENTS: "deployments.yaml"
  ARTIFACT_ICAPSULE_DEPLOY: "icapsule-deployments.yaml"

  MANIFEST_ASSET_FILENAME: "manifest.yaml"

  COMPONENT_CORE: 'core'

  ICAPSULE_PIN: 'true'
  LD_MANIFEST_TEMPLATE: 'manifest.yaml'

  ICAPSULE_CORE_TGZ: "${CI_PROJECT_NAME}-${VERSION}.tar.gz"

  ARTIFACT_MANIFEST: "manifest-${CI_PROJECT_NAME}.yaml"

stages:
  - prepare
  - deploy
  - release

prepare:
  stage: prepare
  script:
    - export TAGNAME=continuous-${CI_COMMIT_BRANCH}

    - echo "export TAGNAME=${TAGNAME}" > $ARTIFACT_ENV
    - echo "export PINNAME=${CI_PROJECT_NAME}-${TAGNAME}-${VERSION}" >> $ARTIFACT_ENV
    - echo "export PINNAME_ICON=${CI_PROJECT_NAME}-icon" >> $ARTIFACT_ENV
    - echo "export ARTIFACT_DEPLOYMENTS_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${TAGNAME}/${ARTIFACT_ICAPSULE_DEPLOY}" >> $ARTIFACT_ENV

    - echo "export ICAPSULE_CORE_TGZ_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${TAGNAME}/$ICAPSULE_CORE_TGZ" >> $ARTIFACT_ENV

  artifacts:
    expire_in: 'never'
    paths:
      - $ARTIFACT_ENV

deploy:
  stage: deploy
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /\[deploy\]/
      when: always
  before_script:
    - source $ARTIFACT_ENV
    - python -m venv $VENV
    - pip install omegaconf
    - pip install ipfshttpclient

    - ipfs version
    - ipfs init
    - ipfs config --json Peering.Peers "$(cat /etc/go-ipfs/ipfs-peering-pinata.json)"
    - nohup ipfs daemon &
    - sleep 5

    - git clone https://gitlab.com/galacteek/icapsule icapsule

    # - wget -O deployments.prev.yaml ${ARTIFACT_DEPLOYMENTS_URL} || true
  script:
    # Add remote service and import
    - echo "Adding RPS with endpoint $IPFS_RPS_ENDPOINT"
    - ipfs pin remote service add default $IPFS_RPS_ENDPOINT $IPFS_RPS_TOKEN

    - >
      if [ ! -z "${DAPP_ICON_FILENAME}" ]; then
        export ICON_CID=$(ipfs add -Q --cid-version 1 share/icons/${DAPP_ICON_FILENAME})
        # ipfs pin remote rm --service=default --name="${PINNAME_ICON}" --force
        ipfs pin remote rm --service=default --cid="${ICON_CID}" --force
        sleep 3
        ipfs pin remote add --service=default --name="${PINNAME_ICON}" $ICON_CID
      fi

    # Create tarball and import it in the repos
    - >
      if [ -d "${COMPONENT_CORE}/qml" ]; then
        tar \
        -C ${COMPONENT_CORE} \
        -X icapsule/.ipfsignore.default \
        -czvf "$ICAPSULE_CORE_TGZ" \
        qml
      else
        tar \
        -C ${COMPONENT_CORE} \
        -X icapsule/.ipfsignore.default \
        -czvf "$ICAPSULE_CORE_TGZ" \
        .
      fi

    - export CORE_CID=$(ipfs add --cid-version 1 -Q "$ICAPSULE_CORE_TGZ")
    - export CORE_DIST_URL="https://gateway.pinata.cloud/ipfs/${CORE_CID}"

    # Manifest
    - echo "CID is $CORE_CID"
    - >
      python3 icapsule/scripts/icapsule-manifestify.py
      --cid $CORE_CID
      --dst $ARTIFACT_MANIFEST
      --template ${LD_MANIFEST_TEMPLATE}

    - MANIFEST_CID=$(ipfs add --cid-version 1 -Q $ARTIFACT_MANIFEST)
    - MANIFEST_DIST_URL="https://gateway.pinata.cloud/ipfs/${MANIFEST_CID}"

    - echo "MANIFEST_DIST_URL=https://gateway.pinata.cloud/ipfs/${MANIFEST_CID}" > $ARTIFACT_DIST

    - echo "Manifest CID is $MANIFEST_CID"

    # Wait some time after adding
    - sleep 15

    # Pin
    - |-
      if [[ "$ICAPSULE_PIN" =~ 'true' ]]; then
        # ipfs pin remote rm --service=default --name="${PINNAME}" --force
        ipfs pin remote rm --service=default --cid="${CORE_CID}" --force
        sleep 1
        ipfs pin remote add --service=default --name="${PINNAME}" $CORE_CID
      fi

    - >
      # ipfs pin remote rm
      # --service=default
      # --name="${PINNAME}-manifest" --force

      ipfs pin remote rm
      --service=default
      --cid="${MANIFEST_CID}" --force

    - >
      ipfs pin remote add
      --service=default
      --name="${PINNAME}-manifest"
      ${MANIFEST_CID}

    - echo "Pinned project ${CI_PROJECT_NAME} to RPS"

  artifacts:
    expire_in: 'never'
    paths:
      - $ARTIFACT_DEPLOYMENTS
      - $ARTIFACT_MANIFEST
      - $ARTIFACT_DIST
      - $ICAPSULE_CORE_TGZ

release:
  image: registry.gitlab.com/gitlab-org/release-cli
  stage: release
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /\[deploy\]/
      when: always
  script:
    - source $ARTIFACT_ENV
    - source $ARTIFACT_DIST
    - apk add bash grep git curl wget

    - >
      curl
      --request DELETE
      --header "JOB-TOKEN: $CI_JOB_TOKEN"
      --header "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}"
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/${TAGNAME}"

    - >
     curl --header "JOB-TOKEN: $CI_JOB_TOKEN"
     --upload-file ${ICAPSULE_CORE_TGZ}
     ${ICAPSULE_CORE_TGZ_URL}
      
    - >
      release-cli create
      --name release-${VERSION}
      --description CHANGELOG.md
      --tag-name $TAGNAME
      --ref $CI_COMMIT_SHA
      --assets-link
      "{\"name\": \"${MANIFEST_ASSET_FILENAME}\", \"filepath\": \"/${MANIFEST_ASSET_FILENAME}\", \"url\": \"${MANIFEST_DIST_URL}\"}"
      --assets-link
      "{\"name\" :\"${ICAPSULE_CORE_TGZ}\", \"url\": \"${ICAPSULE_CORE_TGZ_URL}\"}"
