#!/usr/bin/env python

import sys
import os
import argparse
import traceback
import tarfile
import ipfshttpclient
import tempfile

from omegaconf import OmegaConf

from ipfshttpclient import client


def assert_v(version: str, minimum: str = '0.4.23',
             maximum: str = '0.12.0') -> None:
    pass


client.assert_version = assert_v


parser = argparse.ArgumentParser()
parser.add_argument('--template',
                    dest='tmpl',
                    default='ld-manifest.tmpl.yaml')
parser.add_argument('--ipfs-maddr',
                    dest='maddr',
                    default='/dns/localhost/tcp/5001/http')
parser.add_argument('--dst', dest='dest')
parser.add_argument('--cid', dest='cid')
args = parser.parse_args()

if not args.tmpl:
    print('Invalid options')
    sys.exit(1)


def ocr_ipfsimport(path: str):
    try:
        client = ipfshttpclient.connect(args.maddr)

        res = client.add(path,
                         recursive=True,
                         cid_version=1)
        return res.pop()['Hash']
    except Exception:
        traceback.print_exc()
        return ''


def ocr_encapsulate(source_dir: str):
    try:
        client = ipfshttpclient.connect(args.maddr)

        with tempfile.NamedTemporaryFile() as file:
            with tarfile.open(file.name, 'w:gz') as tar:
                tar.add(source_dir, arcname=os.path.basename(source_dir))

            res = client.add(file.name,
                             cid_version=1)
            return res['Hash']
    except Exception:
        traceback.print_exc()
        return ''


OmegaConf.register_new_resolver("ipfsadd", ocr_ipfsimport)
OmegaConf.register_new_resolver("encapsulate", ocr_encapsulate)


if args.cid:
    os.environ['CID_MAIN'] = args.cid

try:
    manifest = OmegaConf.load(args.tmpl)
    c = OmegaConf.to_container(manifest, resolve=True)

    c['@context'] = {
        '@vocab': 'ips://galacteek.ld/'
        # '@base': 'ips://galacteek.ld/',
    }

    OmegaConf.save(c, args.dest)
except Exception:
    traceback.print_exc()
    sys.exit(1)


sys.exit(0)
