#!/usr/bin/env python

import os
import sys
import uuid
import argparse
import traceback
from datetime import datetime
from omegaconf import OmegaConf
from pathlib import Path


parser = argparse.ArgumentParser()
parser.add_argument('--dst', dest='dest')
parser.add_argument('--manifest-cid', dest='cid')
parser.add_argument('--manifest-previous-path', dest='prev', default=None)
args = parser.parse_args()

if not args.dest or not args.cid:
    print('Invalid options')
    sys.exit(1)

uid = str(uuid.uuid4())
version = os.environ.get('VERSION')
rid = version if version else uid

dplsp = {}
if args.prev:
    p = Path(args.prev)
    if p.is_file():
        try:
            dplsp = OmegaConf.load(str(p))
        except Exception:
            traceback.print_exc()
    else:
        print(f'Ignoring non-existing input file {p}')

dpls = OmegaConf.merge(dplsp, {
    'latest': rid,
    'releases': {
        rid: {
            'date': datetime.now().isoformat(),
            'manifestIpfsPath': args.cid
        }
    }
})

try:
    OmegaConf.save(dpls, args.dest)
except Exception:
    traceback.print_exc()
    sys.exit(1)


sys.exit(0)
