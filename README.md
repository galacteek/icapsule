icapsule
========

*icapsules* are small code packages (mostly written in QML) used
by [galacteek](https://gitlab.com/galacteek/galacteek). They are
distributed on IPFS.

This repository contains the scripts and CI tools necessary to
easily deploy a capsule.

Usage
-----

QML capsule
^^^^^^^^^^^

Add the following to the *.gitlab-ci.yml* file of your capsule
(this uses the *qml-generic* CI template):

```yaml
variables:
  VERSION: '1.0.0'

include:
  remote: 'https://gitlab.com/galacteek/icapsule/-/raw/master/.gitlab/templates/qml-generic.yml'
```

If your capsule uses QML, your code should be located in the *core/qml*
directory for the main component.
